import java.math.BigDecimal;
import java.math.RoundingMode;

public class calculator {
	public static void main(String[] args) {
		BigDecimal q1 = BigDecimal.valueOf(1.02);
		BigDecimal q2 = BigDecimal.valueOf(1.05);
		BigDecimal q3;
		
		BigDecimal f1 = calculateFunction(q1);
		BigDecimal f2 = calculateFunction(q2);
		BigDecimal f3;
		System.out.println("q1 = " + q1.toString());
		System.out.println("f(q1) = " + f1.toString());
		System.out.println("q2 = " + q2.toString());
		System.out.println("f(q2) = " + f2.toString());
		
		for (int i = 3; i < 6; i++) {
			q3 = q2.subtract(f2.multiply((q2.subtract(q1)).divide(f2.subtract(f1), 10, RoundingMode.HALF_UP)));
			f3 = calculateFunction(q3);
			System.out.println("q" + i + " = " + q3.toString());
			System.out.println("f(q" + i + ") = " +f3.toString());
			if (f1.multiply(f2).compareTo(BigDecimal.ZERO) < 0) {
				q1 = q2;
				f1 = f2;
				q2 = q3;
				f2 = f3;
			} else { 
				f1 = f1.multiply(BigDecimal.valueOf(0.5));
				q2 = q3;
				f2 = f3;
			}	
		}
		System.out.print("Programm wurde beendet.");
	}
	
	public static BigDecimal calculateFunction (BigDecimal q) {
		BigDecimal f;
		BigDecimal K = BigDecimal.valueOf(50000);
		BigDecimal A = BigDecimal.valueOf(6000);
		int n = 10;
		
		f = K.multiply(q.pow(n)).subtract(A.multiply((q.pow(n).subtract(BigDecimal.ONE)).divide(q.subtract(BigDecimal.ONE))));
		
		return f;
	}
}
